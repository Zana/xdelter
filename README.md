# About xdelter

xdelter is an open-source, lightweight Graphical User Interface tool for xdelta3 which provides a simple interface to manipulate the xdelta3 executable without actually using the command line.
xdelta3 is an open-souce VCDIFF delta compression tool.

By giving xdelta3 two files to compare, it generates a patch by analyzing the differences between the first and second file. When using this patch with xdelta3 on the first file, an exact binary copy of the second file is created.

One possible use : You have an MKV video file with subtitles inside. Later, you release a second version of that MKV file with better subtitles (typos were fixed, etc). The video stream itself is actually unchanged, only the subtitles were modified. However, many users would have to download the whole new video, as the subtitles are embedded inside the MKV container, and most people do not know how to merge the subtitles alone into an MKV container.
By using xdelta3, you can compare the two MKV files and generate a small patch that only weighs a few kilobytes of data. Later, any user can use xdelta3 and the patch to turn the first version of the MKV file into the second, without having to download the whole video again.

Other possible uses : Patching ISO files, Patching large dump files, etc.

# How to download & install

* Download the latest version [here](http://static.neregate.com/softwares/xdelter/xdelter%20v1.0.zip).
	- Latest version : 1.0
	- Archive name : xdelter v1.0.zip
	- md5 checksum : ``2009ec1c04cb8c3dee7fc50c127475a8``
	- sha1 checksum : ``fa49a51d52720af1485a1ea840b0ea0a54eb0161``
* Unzip the contents in any directory on your hard drive.
* Run xdelter.exe

**IMPORTANT : Some anti-virus softwares may flag xdelter.exe as a virus. This is a false positive.
The problem comes from the AutoIt language itself, which is often used in online games to program bots. If you do not trust the executable, you can check and compile the source yourself (see below).**

# How to use

1. Select an input file (version 1 of the file you want to create a patch from). Alternatively, you can drag and drop a file from your FileSystem into the input field, and its path will be automatically added.
2. Select an output file (version 2 of the file you want to create a patch from). Drag and drop feature also works on this field.
3. Select a save location and a name for your patch file.
4. Specify an organization/company name (optional). This field will be added to the generated batch file (see below), if you decide to create one.
5. Tick the extra options you want to use :
	* Zip Archive : Wraps your generated patch file, the xdelta3 executable and optionally the batch file into a zip archive for easy sharing.
	* Batch file : Create a batch (.bat) script along with the patch. Doing this will ease the patching process for the users you distribute the patch to, as they will just have to run the batch file and follow the instructions to patch their file, instead of using the xdelta3 command line. It is recommended to use this option together with the first option for the best sharing experience.
	* Identical file path : If checked, the program will always match the directory of the patch file with the directory of the output file. Please note that the option will not apply if the output file was already selected. You need to select the output file again for the path to be changed.
6. Click on the "Create Patch" button to create the patch.

# How to compile

1. Download or clone the xdelter repository.
2. Download and install the Full installation package of AutoIt [here](http://www.autoitscript.com/site/autoit/downloads/).
3. Open the ``xdelter.au3`` file in the SciTE editor bundled with your AutoIt installation.
4. In the editor, press ``CTRL+F7`` to bring out the compilation window.
5. Click on the "Compile Script" button to create the xdelter executable.

# How to contribute

The figure below shows the merge & commit workflow.

![xdelter commit & merge workflow](http://media.neregate.com/i/xdelter_workflow.png "xdelter commit & merge workflow")

* If you wish to contribute, you can request an SSH access to the "development" branch by sending me an email (zana at neregate • com), or send me the script file with your changes & your contributor nickname.
* If you want to receive SSH access, you will need to provide me with a public SSH-2 RSA key (2048 bits). Please put your contributor nickname in the Key's comment section. I will assume you know how to create and use a key pair, and I recommend you use the [PuTTY Key Generator](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) (puttygen) program to generate the needed keys. Do not forget to check the md5/sha1 checksums before using the program.
* Once I have added your key to the authorized contributors keys, you can clone the project on your computer to test your SSH access (the SSH connection URL will be given to you by email).
* Contributors develop on the "development" branch only. Before you start working on a feature or bugfix, please [create a new issue](http://tracker.neregate.com/browse/XDLT/) in the tracker so that everyone knows who works on what.
* Try to use your nickname and optionally your email when doing a commit, it helps with the commit history (but be aware that your mail address will be exposed, so it's up to you).
* In the commit's comment, please put your nickname before your comment : ``[MY_NICKNAME] This is a commit message.``
* I will be doing the merges and commit approvals accross all the branches. If someone puts on a feature request or reports a bug on the tracker, I will usually handle it (unless you want to handle the incoming flow) using the "bugfix" and "release" branches (those are only for me).
* If you no longer wish to contribute, please be kind enough to notify me so that I can remove your SSH public key from the authorized keys.
* **SSH access keys are *NOT* meant to be shared. The access is for you, and you alone. Same for the SSH URL. Please have the correction of upholding the trust that is given to you. If you are a coder like I am, you understand how important that is. Obviously, every SSH access and action is logged.**

# I found a bug / I got an idea

Sure thing! Feel free to [create an issue](http://tracker.neregate.com/) about it on the tracker (the big Create issue button on the top menubar).

# FAQ

**Q** : Can I have my own Git user? I could make pull requests that way! <br/>
**A** : Unfortunately, due to license limitations, I cannot have more than 10 users. That speaks for itself.

**Q** : Can I put my nickname when commenting instead of being Anonymous? <br/>
**A** : Not yet. In the near future, I hope. But feel free to put your nickname at the bottom of your comment.

**Q** : Why not use another tool for repository management and bug tracking, free and without limitations? <br/>
**A** : Because I never do things the conventional way. I like to try new things as well (main reason).

**Q** : That doesn't make it any easier to contribute. <br/>
**A** : I know that very well. I am not expecting contributions to be honest, but I wanted a place in which I had full control over the application I deploy; something mature that I can enjoy using, and a place for you to browse and download the source as well.