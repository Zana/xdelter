#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=resources\Icon.ico
#AutoIt3Wrapper_Outfile=xdelter.exe
#AutoIt3Wrapper_UseUpx=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <EditConstants.au3>
#include <File.au3>
#include <FileConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#cs ----------------------------------------------------------------------------

	xdelter - open-source Graphical User Interface for xdelta3
    Copyright (C) 2013-2014 neregate.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ce ----------------------------------------------------------------------------

#Region === CONSTANTS ==========================================================
Local Const $VERSION		= "1.0"
Local Const $VERSION_INFO	= ""
Local Const $UTILS_PATH		= @ScriptDir & "\utils\"
Local Const $XDELTA_EXE		= "xdelta3.exe"
Local Const $7Z_EXE			= "7za.exe"
Local Const $CONFIG_PATH	= @ScriptDir & "\Config.ini"
#EndRegion =====================================================================

#Region === VARIABLES ==========================================================
Local $hInputFile, $hOutputFile, $hPatchFile, $hCompanyName
Local $hButtonSelectInputPath, $hButtonSelectOutputPath, $hButtonSelectPatchPath
Local $hConsole, $hCboxZip, $hCboxBatch, $hCboxPatchPath
Local $hCreatePatch, $hClear, $hExit
#EndRegion =====================================================================



_Start()



; #FUNCTION# ====================================================================================================================
; Name ..........: _Start
; Description ...: Creates the main UI and waits for user actions.
; Syntax ........: _Start()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Start()

	_CreateUI()

	While 1
		Switch GUIGetMsg()
			Case $GUI_EVENT_CLOSE, $hExit
				_SaveSettings()
				Exit
			Case $GUI_EVENT_DROPPED
				Switch @GUI_DropId
					Case $hInputFile
						GUICtrlSetData($hInputFile, @GUI_DragFile)
					Case $hOutputFile
						GUICtrlSetData($hOutputFile, @GUI_DragFile)
						_SetPatchPath()
				EndSwitch
			Case $hButtonSelectInputPath
				_SelectInputFile()
			Case $hButtonSelectOutputPath
				_SelectOutputFile()
			Case $hButtonSelectPatchPath
				_SelectPatchFile()
			Case $hCreatePatch
				_CreatePatch()
			Case $hClear
				_Clear()
		EndSwitch
	WEnd
EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _CreateUI()
; Description ...: Creates the main GUI.
; Syntax ........: _CreateUI()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _CreateUI()

	GUICreate("xdelter v" & $VERSION & " " & $VERSION_INFO, 600, 600, -1, -1, -1, $WS_EX_ACCEPTFILES + $WS_EX_TOPMOST)

	GUICtrlCreateGroup("Input File :", 10, 10, 580, 50)
		$hInputFile = GUICtrlCreateInput("", 20, 25, 470, 25, $ES_READONLY + $GUI_SS_DEFAULT_INPUT)
		GUICtrlSetState(-1, $GUI_DROPACCEPTED)
		$hButtonSelectInputPath = GUICtrlCreateButton("Open...", 500, 25, 80, 25)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	GUICtrlCreateGroup("Output File :", 10, 70, 580, 50)
		$hOutputFile = GUICtrlCreateInput("", 20, 85, 470, 25, $ES_READONLY + $GUI_SS_DEFAULT_INPUT)
		GUICtrlSetState(-1, $GUI_DROPACCEPTED)
		$hButtonSelectOutputPath = GUICtrlCreateButton("Open...", 500, 85, 80, 25)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	GUICtrlCreateGroup("Patch File :", 10, 130, 580, 50)
		$hPatchFile = GUICtrlCreateInput("", 20, 145, 470, 25, $ES_READONLY + $GUI_SS_DEFAULT_INPUT)
		$hButtonSelectPatchPath = GUICtrlCreateButton("Save...", 500, 145, 80, 25)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	GUICtrlCreateGroup("Organization/Company Name :", 10, 190, 580, 50)
		$hCompanyName = GUICtrlCreateInput("", 20, 205, 560, 25)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	GUICtrlCreateGroup("Console Output :", 10, 250, 580, 180)
		$hConsole = GUICtrlCreateEdit("", 20, 265, 560, 155, $ES_READONLY + $ES_AUTOVSCROLL + $WS_VSCROLL)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	GUICtrlCreateGroup("Options :", 10, 440, 580, 90)
		$hCboxZip = GUICtrlCreateCheckbox("Wrap the generated output in a Zip archive", 20, 460, 400, 20)
		$hCboxBatch = GUICtrlCreateCheckbox("Create a Batch script to automate the patching process", 20, 480, 400, 20)
		$hCboxPatchPath = GUICtrlCreateCheckbox("Always make the Patch File path identical to the Output File path", 20, 500, 400, 20)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	GUICtrlCreateGroup("", 10, 540, 580, 50)
		$hCreatePatch = GUICtrlCreateButton("Create Patch", 270, 555, 100, 25)
		$hClear = GUICtrlCreateButton("Clear", 375, 555, 100, 25)
		$hExit = GUICtrlCreateButton("Exit", 480, 555, 100, 25)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	_LoadSettings()

	GUISetState()

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _LoadSettings
; Description ...: Reads the configuration file and sets the company name and checkboxes to the last used value.
; Syntax ........: _LoadSettings()
; Parameters ....:
; Return values .: None
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _LoadSettings()

	GUICtrlSetData( $hCompanyName , IniRead( $CONFIG_PATH, "OPTIONS", "COMPANY_NAME", "") )

	Local $iSettingZip = Int ( IniRead( $CONFIG_PATH, "OPTIONS", "ZIP_ARCHIVE", 1) )
	GUICtrlSetState( $hCboxZip , ($iSettingZip = 1 Or $iSettingZip = 4) ? $iSettingZip : 1 )

	Local $iSettingBatch = Int ( IniRead( $CONFIG_PATH, "OPTIONS", "BATCH_SCRIPT", 1) )
	GUICtrlSetState( $hCboxBatch , ($iSettingBatch = 1 Or $iSettingBatch = 4) ? $iSettingBatch : 1 )

	Local $iSettingSamePath = Int ( IniRead( $CONFIG_PATH, "OPTIONS", "SAME_PATH", 4) )
	GUICtrlSetState( $hCboxPatchPath , ($iSettingSamePath = 1 Or $iSettingSamePath = 4) ? $iSettingSamePath : 4 )

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _SaveSettings
; Description ...: Saves the company name and the checkboxes' state to the configuration file.
; Syntax ........: _SaveSettings()
; Parameters ....:
; Return values .: None
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SaveSettings()

	IniWrite( $CONFIG_PATH, "OPTIONS", "COMPANY_NAME", GUICtrlRead( $hCompanyName ) )

	IniWrite( $CONFIG_PATH, "OPTIONS", "ZIP_ARCHIVE", GUICtrlRead( $hCboxZip ) )

	IniWrite( $CONFIG_PATH, "OPTIONS", "BATCH_SCRIPT", GUICtrlRead( $hCboxBatch ) )

	IniWrite( $CONFIG_PATH, "OPTIONS", "SAME_PATH", GUICtrlRead( $hCboxPatchPath ) )

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _CreatePatch
; Description ...: Creates the Patch File. Optionally creates a Batch (.bat) script to ease the patching process.
; The content can be wrapped into a Zip (.zip) archive as well.
; Syntax ........: _CreatePatch()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _CreatePatch()

	#Region === Checking inputs =================================================
	If GUICtrlRead($hInputFile) = "" Then
		MsgBox(0 + 48 + 262144, "xdelter", "The Input File section is empty!" & @CRLF & "Please select an input file and try again.")
		Return
	ElseIf GUICtrlRead($hOutputFile) = "" Then
		MsgBox(0 + 48 + 262144, "xdelter", "The Output File section is empty!" & @CRLF & "Please select an output file and try again.")
		Return
	ElseIf GUICtrlRead($hPatchFile) = "" Then
		MsgBox(0 + 48 + 262144, "xdelter", "The Patch File section is empty!" & @CRLF & "Please select an output location for the patch file and try again.")
		Return
	ElseIf GUICtrlRead($hInputFile) = GUICtrlRead($hOutputFile) Then
		MsgBox(0 + 48 + 262144, "xdelter", "The input file is identical to the output file!" & @CRLF & "Trust me, that's not going to work.")
		Return
	EndIf
	#EndRegion===================================================================

	Local $sWorkingDir, $sFileName
	Local $PatchFilePathNoExt = StringTrimRight(GUICtrlRead($hPatchFile), 4)

	#Region === XDELTA Patch Creation ===========================================
	GUICtrlSetState($hCreatePatch, $GUI_DISABLE)
	If FileExists(GUICtrlRead($hPatchFile)) Then FileDelete(GUICtrlRead($hPatchFile))
	_AppendToConsole("Creating patch file, please wait...")
	RunWait(@ComSpec & ' /c ' & $XDELTA_EXE & ' -9 -f -s "' & GUICtrlRead($hInputFile) & '" "' & GUICtrlRead($hOutputFile) & '"  "' & GUICtrlRead($hPatchFile) & '"' , $UTILS_PATH, @SW_HIDE)
	If Not FileExists(GUICtrlRead($hPatchFile)) Then
		_AppendToConsole("Patch creation failed.")
		GUICtrlSetState($hCreatePatch, $GUI_ENABLE)
		Return
	Else
		_AppendToConsole("Patch created successfully.")
	EndIf
	#EndRegion===================================================================

	#Region === Batch Script Creation ===========================================
	If GUICtrlRead($hCboxBatch) = $GUI_UNCHECKED Then
		_AppendToConsole("Skipping batch file creation.")
	Else
		_AppendToConsole("Creating batch file...")
		Local $hBatchFile = FileOpen($PatchFilePathNoExt & ".bat", 2 + 256) ; OVERWRITE + UTF-8 (without BOM)
		If $hBatchFile = -1 Then
			_AppendToConsole("Could not open batch file in write mode.")
			GUICtrlSetState($hCreatePatch, $GUI_ENABLE)
			Return
		EndIf
		_SplitPath(GUICtrlRead($hInputFile), $sWorkingDir, $sFileName)
		FileWriteLine($hBatchFile, '@ECHO OFF')
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		FileWriteLine($hBatchFile, 'ECHO VCDIFF Patch for ' & $sFileName)
		If Not GUICtrlRead($hCompanyName) = "" Then	FileWriteLine($hBatchFile, 'ECHO By ' & GUICtrlRead($hCompanyName))
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		FileWriteLine($hBatchFile, 'ECHO Created using xdelter ' & $VERSION)
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		FileWriteLine($hBatchFile, 'ECHO  -')
		FileWriteLine($hBatchFile, 'ECHO Please make sure that the input file is located in the same')
		FileWriteLine($hBatchFile, 'ECHO directory as this script.')
		FileWriteLine($hBatchFile, 'ECHO Once you are ready to apply the patch, press any key to begin.')
		FileWriteLine($hBatchFile, 'ECHO  -')
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		FileWriteLine($hBatchFile, 'PAUSE')
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		FileWriteLine($hBatchFile, 'ECHO Now creating the patched file. Do not close this window.')
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		_SplitPath(GUICtrlRead($hPatchFile), $sWorkingDir, $sFileName)
		FileWriteLine($hBatchFile, 'ECHO  -')
		FileWriteLine($hBatchFile, 'xdelta3 -n -d ' & $sFileName)
		FileWriteLine($hBatchFile, 'ECHO  -')
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		FileWriteLine($hBatchFile, 'if errorlevel 1 (')
		FileWriteLine($hBatchFile, 'ECHO An error occured during the patch process. Please try again.')
		FileWriteLine($hBatchFile, ') else (')
		FileWriteLine($hBatchFile, 'ECHO Patch file created successfully. You may now close this window.')
		FileWriteLine($hBatchFile, ')')
		FileWriteLine($hBatchFile, 'ECHO ====================================================================')
		FileWriteLine($hBatchFile, 'PAUSE')
		FileClose($hBatchFile)
		_AppendToConsole("Batch file created successfully.")
	EndIf

	#EndRegion===================================================================

	#Region === Zip File Creation ===============================================
	If GUICtrlRead($hCboxZip) = $GUI_UNCHECKED Then
		_AppendToConsole("Skipping zip archive creation.")
		_AppendToConsole("Remember to include the xdelta3 executable if you plan on distributing the patch.")
	Else
		_AppendToConsole("Creating zip archive...")
		_SplitPath(GUICtrlRead($hPatchFile), $sWorkingDir, $sFileName)
		Local $sCommandLine = $7Z_EXE & ' a "' & $sWorkingDir & StringTrimRight($sFileName, 4) & '.zip" "' & GUICtrlRead($hPatchFile) & '"'
		If GUICtrlRead($hCboxBatch) = $GUI_CHECKED Then $sCommandLine &= ' -i!"' & $PatchFilePathNoExt & '.bat"'
		$sCommandLine &= ' -i!' & $XDELTA_EXE & ' -sccUTF-8'
		Local $iPID = Run(@ComSpec & ' /c ' & $sCommandLine, $UTILS_PATH, @SW_HIDE, $STDOUT_CHILD)

		Local $sLine
		Local $bZipOK = False
		While ProcessExists($iPID)
			$sLine = StdoutRead($iPID)
			If StringInStr($sLine, "Everything is Ok") <> 0 Then $bZipOK = True
			If Not @error And Not $sLine = "" Then GUICtrlSetData($hConsole, $sLine & @CRLF, 1)
		WEnd
		StdioClose($iPID)

		; Delete .xsf & .bat files regardless of the Zip state
		_AppendToConsole("Cleaning up...")
		If GUICtrlRead($hCboxBatch) = $GUI_CHECKED Then FileDelete($PatchFilePathNoExt & ".bat")
		FileDelete(GUICtrlRead($hPatchFile))

		If $bZipOK Then
			_AppendToConsole("Zip archive created successfully.")
		Else
			_AppendToConsole("An error occured during the creation of the archive.")
		EndIf

	EndIf
	#EndRegion===================================================================

	_AppendToConsole("The patch creation process is complete.")
	_AppendToConsole("================================================================")
	GUICtrlSetState($hCreatePatch, $GUI_ENABLE)

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _AppendToConsole
; Description ...: Appends a timestamped line to the console output control ($hConsole).
; Syntax ........: _AppendToConsole($sString)
; Parameters ....: $sString             - The string to append to the console.
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _AppendToConsole($sString)

	GUICtrlSetData($hConsole, @HOUR & ":" & @MIN & ":" & @SEC & ":" & @MSEC & " -> " & $sString & @CRLF, 1)

EndFunc

; #FUNCTION# ====================================================================================================================
; Name ..........: _SelectInputFile
; Description ...: Opens a dialog, allowing the user to pick the source file used to create the patch.
; Syntax ........: _SelectInputFile()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SelectInputFile()

	Local $sWorkingDir = @DocumentsCommonDir
	Local $sInputFileName = ""

	If Not GUICtrlRead($hInputFile) = "" Then
		_SplitPath(GUICtrlRead($hInputFile), $sWorkingDir, $sInputFileName)
	EndIf

	Local $hFile = FileOpenDialog ("Select the input file :", $sWorkingDir, "Any file type (*.*)", $FD_FILEMUSTEXIST + $FD_PATHMUSTEXIST, $sInputFileName)
	If Not @error Then
		GUICtrlSetData($hInputFile, $hFile)
		GUICtrlSetTip($hInputFile, GUICtrlRead($hInputFile))
	EndIf

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _SelectOutputFile
; Description ...: Opens a dialog, allowing the user to pick the target file used to create the patch.
; Syntax ........: _SelectOutputFile()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SelectOutputFile()

	Local $sWorkingDir = @DocumentsCommonDir
	Local $sOutputFileName = ""

	If Not GUICtrlRead($hOutputFile) = "" Then
		_SplitPath(GUICtrlRead($hOutputFile), $sWorkingDir, $sOutputFileName)
	EndIf

	Local $hFile = FileOpenDialog ("Select the output file :", $sWorkingDir, "Any file type (*.*)", $FD_FILEMUSTEXIST + $FD_PATHMUSTEXIST, $sOutputFileName)
	If Not @error Then
		GUICtrlSetData($hOutputFile, $hFile)
		GUICtrlSetTip($hOutputFile, GUICtrlRead($hOutputFile))
		_SetPatchPath()
	EndIf

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _SelectPatchFile
; Description ...: Opens a dialog, allowing the user to save the patch file at a specific location.
; Syntax ........: _SelectPatchFile()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SelectPatchFile()

	Local $sWorkingDir = @DocumentsCommonDir
	Local $sPatchFileName = "Patch.xpf"

	If Not GUICtrlRead($hPatchFile) = "" Then
		_SplitPath(GUICtrlRead($hPatchFile), $sWorkingDir, $sPatchFileName)
	EndIf

	Local $hFile = FileSaveDialog ("Save the generated patch file in :", $sWorkingDir, "xdelter Patch File (*.xpf)", $FD_PATHMUSTEXIST + $FD_PROMPTOVERWRITE, $sPatchFileName)
	If Not @error Then
		GUICtrlSetData($hPatchFile, $hFile)
		GUICtrlSetTip($hPatchFile, GUICtrlRead($hPatchFile))
	EndIf

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _SetPatchPath
; Description ...: Sets the path and name of the patch file in its input control ($hPatchFile). Patch name defaults to Patch.xpf.
; Syntax ........: _SetPatchPath()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SetPatchPath()

	Local $sWorkingDir, $PatchFileName, $sTempFileName
	Local $PatchFileName = "Patch.xpf"

	If GUICtrlRead($hCboxPatchPath) = $GUI_CHECKED Then
		If Not GUICtrlRead($hPatchFile) = "" Then _SplitPath(GUICtrlRead($hPatchFile), $sWorkingDir, $PatchFileName)
		_SplitPath(GUICtrlRead($hOutputFile), $sWorkingDir, $sTempFileName)
		GUICtrlSetData($hPatchFile, $sWorkingDir & $PatchFileName)
		GUICtrlSetTip($hPatchFile, $sWorkingDir & $PatchFileName)
	EndIf

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _SplitPath
; Description ...: Splits a full file path into a path and a filename.
; Syntax ........: _SplitPath($sFilePath, Byref $sWorkingDir, Byref $sResultingFileName)
; Parameters ....: $sFilePath           - The file path to split.
;                  $sWorkingDir         - [in/out] The directory containing the file.
;                  $sResultingFileName  - [in/out] The file name and its extension, without the path.
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......: Both the file name and directory variables passed as arguments are directly modified by this function.
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _SplitPath($sFilePath, ByRef $sWorkingDir, ByRef $sResultingFileName)

	Local $sDrive = "", $sDir = "", $sFileName = "", $sExtension = ""
	Local $asPathSplit = _PathSplit($sFilePath, $sDrive, $sDir, $sFileName, $sExtension)
	$sWorkingDir = $asPathSplit[1] & $asPathSplit[2]
	$sResultingFileName = $asPathSplit[3] & $asPathSplit[4]

EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _Clear
; Description ...: Clears the contents of the Input/Output/Patch/Console controls.
; Syntax ........: _Clear()
; Parameters ....:
; Return values .:
; Author ........: Zana
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Clear()

	GUICtrlSetData($hInputFile, "")
	GUICtrlSetData($hOutputFile, "")
	GUICtrlSetData($hPatchFile, "")
	GUICtrlSetData($hConsole, "")

EndFunc